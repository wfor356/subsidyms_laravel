<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->id();
            $table->biginteger('user_id');
            $table->date('purchase');
            $table->morphs('name');
            $table->mediumInteger('value');
            $table->text('purpose');
            $table->text('note');
            $table->timestampTz('deleted_at');
            $table->date('request');

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
};
