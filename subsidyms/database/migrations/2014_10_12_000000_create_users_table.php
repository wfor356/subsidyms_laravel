<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->biginteger('id')->unique();
            $table->biginteger('mms_id');
            $table->DATE('ym');
            $table->mediuminteger('perfect');
            $table->mediuminteger('pre_amount')->nullable();
            $table->mediuminteger('tranferred');
            $table->timestamp('deleted_at')->useCurrent()->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
