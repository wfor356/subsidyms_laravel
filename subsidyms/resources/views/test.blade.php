

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    {{Form::select('事業所', ['全事業所', 'ブルーマーリン', 'ホワイトマーリン','オレンジマーリン', 'グリーンマーリン','ネイビーマーリン','イエローマーリン','グランドマーリン','レインボーマーリン'])}}
    {{Form::select('年度', ['2022', '2021', '2020', '2019'])}}年度
    {{Form::select('月度', ['4', '5', '6', '7','8','9','10','11','12','1','2','3'])}}月
  <form method="post">
    @csrf
    <input type="submit" name="button1" value="ボタン1">
    <input type="submit" name="button2" value="ボタン2">
  </form>

    <table>
        <tr><th>id</th><th>mms_id</th><th>ym</th></tr>
        @foreach ($items as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->mms_id}}</td>
                <td>{{$item->ym}}</td>
            </tr>
        @endforeach
    </table>
 
</body>
</html>